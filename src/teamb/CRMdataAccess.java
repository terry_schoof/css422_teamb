package teamb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class CRMdataAccess {

	//Query types
	public final static int QUERY_BY_NAME = 0;
	public final static int QUERY_BY_ADDRESS = 1;
	public final static int QUERY_BY_CITY = 2;
	public final static int QUERY_BY_STATE = 3;
	public final static int QUERY_BY_ZIP = 4;
	public final static int QUERY_BY_COUNTRY = 5;
	public final static int QUERY_BY_PHONE = 6;
	public final static int QUERY_BY_ID = 7;
	public final static int QUERY_ALL = 8;
	
	private String derby_port = "1527";
	private String derby_host = "localhost";
	

	
	

	//Create string for database, this will be ran for users to "Import" the database.
    private String DB_HOST_CREATE = null;
    
    //String for database
    private String DB_HOST = null;
    
    //SQL statement to create user table if it doesn't exist
    private final String SQL_TBL = "CREATE TABLE CONTACTS("
    								+ "Name varchar(15),"
    								+ "Address varchar(15),"
    								+ "City varchar(30),"
    								+ "state_prov varchar(10),"
    								+ "ZIP varchar(5),"
    								+ "Country varchar(3),"
    								+ "phone varchar(12),"
    								+ "CUST_ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1))";
    
    //common SQL string used to retrieve all contacts
    private final String SQL_SEL = "SELECT * FROM CONTACTS";
    
    //Base SQL statement to select a specific row
    private final String SQL_ROWSEL = "select * from CONTACTS where CUST_ID = ?";
    
    //Base SQL statement to insert a contact to table
    private final String SQL_INSERT ="Insert into CONTACTS(Name,Address,City,state_prov,ZIP,Country,phone) values (?,?,?,?,?,?,?)";
    
    //base SQL statment to update contact info from Jtable ***row needs to be added before execution
    private final String SQL_UPDT = "UPDATE CONTACTS SET Name = ?,"
    							  + "Address = ?,"
    							  + "City = ?,"
    							  + "state_prov = ?,"
    							  + "ZIP = ?,"
    							  + "Country = ?,"
    							  + "phone = ?"
    							  + " WHERE CUST_ID = ?";
    
    private PreparedStatement pst = null;
    private Connection conn = null;
    private Statement stmt = null;
    
    //bare constructor, sets up default values for host and port
    public CRMdataAccess()
	{
		initVars(); 
		checkTable();
	}
    
    //constructor to be used is using custom derby host and port
    public CRMdataAccess(String port, String hostname)
	{		
		this.derby_host = hostname;
		this.derby_port = port;
		initVars();
		checkTable();
	}
	
    private void initVars()
    {
    	DB_HOST_CREATE = "jdbc:derby://"
    				   	+ derby_host
    				   	+ ":"
				  		+ derby_port
				  		+ "/TeamBContacts;create=true";
    	
    	DB_HOST = "jdbc:derby://"
    			+ derby_host
    			+ ":"
				+ derby_port
				+ "/TeamBContacts";
    }
	
	/****************************************************************************************************
	 * 
	 * function: findRecord(filter, qureytype)
	 * 
	 * Returns: result set
	 * 
	 * main public function to return info depending on the queryType
	 * 
	 * Query Types are as follows:
	 * 
	 * CRMdataAccess.QUERY_BY_NAME
	 * CRMdataAccess.QUERY_BY_ADDRESS
	 * CRMdataAccess.QUERY_BY_CITY
	 * CRMdataAccess.QUERY_BY_STATE
	 * CRMdataAccess.QUERY_BY_ZIP
	 * CRMdataAccess.QUERY_BY_COUNTRY
	 * CRMdataAccess.QUERY_BY_PHONE
	 * CRMdataAccess.QUERY_BY_ID
	 * CRMdataAccess.QUERY_ALL
	 * 
	 * the filter will search for any record that matches what the filter contains and is case sensitive.
	 * Except the STATE query, for this query you have to use the two letter code of the state.
	 * */	
	public ResultSet findRecord(String filter, int queryType) throws SQLException
	{
		ResultSet rs = null;
		String sql = formSQL(filter, queryType);
		
		//Open connection to DB_HOST
		conn = DriverManager.getConnection(DB_HOST);
		
		//default createStatment
		stmt = conn.createStatement();
		
		
		rs = stmt.executeQuery(sql);
		//reinitSQL();
		return rs;		
	}//end findRecord
	
	/****************************************************************************************************
	 * 
	 * function: updateRecord
	 * 
	 * Returns: True if successful, False if not successful
	 * 
	 * main public function to update an entry in the Users table
	
	 * */
	public boolean updateRecord(String name,
								String address, 
								String city, 
								String state, 
								String ZIP, 
								String Country, 
								String phone, 
								String CUST_ID) 
										throws SQLException
	{
		conn = DriverManager.getConnection(DB_HOST);
		pst = conn.prepareStatement(SQL_UPDT);
		pst.setString(1, name);
		pst.setString(2, address);
		pst.setString(3, city);
		pst.setString(4, state);
		pst.setString(5, ZIP);
		pst.setString(6, Country);
		pst.setString(7, phone);
		pst.setString(8, CUST_ID);
		boolean result;
		result = pst.execute();
		reinitSQL();
		return result; 
	}

	/*****************************************************************************************
	 * 
	 *  function: sqlQuery(String sql)
	 *  
	 *  returns: ResultSet
	 *  
	 * This function is used to run a custom sql SELECT query against the user database.
	 * Note that the database contains one table with the following columns:
	 * Name 
	 * Address
	 * city
	 * state_prov
	 * ZIP
	 * Country
	 * Phone
	 * CUST_ID 
	 * 
	 * The query returns a ResultSet that must be handled in your own code.
	 * 
	 * NOTE::If you attempt an UPDATE, DELETE, or INSERT statement the function will return NULL.	
	 */
	public ResultSet sqlQuery(String sql) throws SQLException
	{
		
		ResultSet rs = null;
		if(sql.toUpperCase().contains("UPDATE")||sql.toUpperCase().contains("DELETE")||sql.toUpperCase().contains("INSERT")) return rs;
		//Open connection to DB_HOST
		conn = DriverManager.getConnection(DB_HOST);
		
		//default createStatment
		stmt = conn.createStatement();
		
		rs = stmt.executeQuery(sql);
		
		/*switch(QueryType)
		{
		case (CRMdataAccess.ADD_RECORD):
		case (CRMdataAccess.UPDATE_RECORD):
		case (CRMdataAccess.FIND_RECORD):
		}*/
		//reinitSQL();
		return rs;
	}
 	
	/*
	 * private function used to create sql statement based on querytype submitted for findrecord function
	 * 
	 * 
	 */
	private String formSQL(String filter, int queryType)
	{
		String sql = "SELECT * FROM CONTACTS WHERE ";
		filter = filter.toUpperCase();
		
		//form the sql statement based on queryType
		switch(queryType)
		{
			case CRMdataAccess.QUERY_BY_CITY:
				sql =  sql + "city LIKE '" + filter + "'";
				break;
			case CRMdataAccess.QUERY_BY_COUNTRY:
				sql =  sql + "country LIKE '" + filter + "'";
				break;
			case CRMdataAccess.QUERY_BY_ID:
				sql =  sql + "ID = '" + filter + "'";
				break;
			case CRMdataAccess.QUERY_BY_NAME:
				sql =  sql + "name = '" + filter + "'";
				break;
			case CRMdataAccess.QUERY_BY_PHONE:
				filter = filter.replace("-", "").replace("(","").replace(")","");
				sql =  sql + "phone = '" + filter + "'";
				break;
			case CRMdataAccess.QUERY_BY_STATE:
				sql =  sql + "state_prov = '" + filter + "'";
				break;
			case CRMdataAccess.QUERY_BY_ZIP:
				sql =  sql + "ZIP = '" + filter + "'";
				break;
			case CRMdataAccess.QUERY_ALL:
				sql = "SELECT * FROM CONTACTS";
		}
		System.out.println(sql);
		return sql;
		
	}
	
	private void checkTable()
    {
    	try 
    	{
    		//Open connection to DB_HOST
    		conn = DriverManager.getConnection(DB_HOST);
			
    		//default createStatment
    		stmt = conn.createStatement();
		} 
    	catch (SQLException e) 
		{			
			JOptionPane.showMessageDialog
			(null, 
			 "Unable to connect to derby,"
		   + " port is set to:" + derby_port 
	       + " host is set to:" + derby_host
		   + ". Please use: new CRMdataAccess(port, hostname) constructor to configure.",
		     "CRMdataAccess", 
			  JOptionPane.WARNING_MESSAGE);
		
			e.printStackTrace();
			System.exit(69);
		}
    	
    	try
    	{
    		//attempt basic query of contacts table
    		stmt.executeQuery(SQL_SEL);
    		
    	//if there is an issue, we can assume it is because the table does not exist.
    	} 
    	catch (SQLException SQLe)
    	{    		
				try 
				{					
					//SQL_tbl statment is to create the table with the specified parameters in the SQL_tbl string.
					stmt.execute(SQL_TBL);					
				}//if this fails, API is useless, inform user with fatal error and exit. 
				catch (SQLException e) 
				{
					JOptionPane.showMessageDialog
					(null,
					 "FATAL ERROR: Cannot connect to contacts table on database."
				   + derby_host 
                   + ":" 
			       + derby_port 
		           + "\n" 
				   + e.getMessage(), 
					 "CRMdataAccess", 
					 JOptionPane.ERROR_MESSAGE);
				}
    	}
    	try 
    	{
			reinitSQL();
		}
    	catch (SQLException e) 
    	{
			e.printStackTrace();
		}
    }
    
    void reinitSQL() throws SQLException 
    {
    	if(pst != null) pst.close();
    	if(stmt != null) stmt.close();
		if(conn != null) conn.close();
		pst = null;
    	stmt = null;
		conn = null;
	}

	public String getPort() 
	{
		return derby_port;
	}
	
	//set custom port if running local networkServer on a custom port.
	public void setPort(String derby_port)
	{
		this.derby_port = derby_port;
	}
	
	public String getDerby_host()
	{
		return derby_host;
	}
	
	//set custom hostname
	public void setDerby_host(String derby_host)
	{
		this.derby_host = derby_host;
	}
}
