package CSS422TeamBrev1;

/*
 Name: Learning Team B
 Assignment: Learning Team Assignment
 Class: CSS/422
 School: University of Phoenix
 Due Date: May 07, 2014
 Program Name: LearningTeamB.java
 */

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Formatter;
import java.util.Scanner;
import java.util.Stack;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;

public class LearningTeamB extends JFrame implements ActionListener {

    private Float amount1;
    
    //Create Label Showing "First Name:"
    private final JLabel firstName = new JLabel("First Name*:");
    
    //Create a text field to populate a first name
    private final JTextField firstNameField = new JTextField(15);
    
    //Create Label Showing "Last Name:"
    private final JLabel lastName = new JLabel("Last Name*:");
    
    //Create a text field to populate a last name
    private final JTextField lastNameField = new JTextField(15);
    
    //Create Label Showing "Charity"
    private final JLabel charity = new JLabel("Charity*:");
    
    //Create a text field to populate a charity
    private final JTextField charityField = new JTextField(30);
    
    //Create Label Showing "Amount Pledged"
    private final JLabel amount = new JLabel("Amount Pledged*:");
       
    //Create a text field to populate an Amount Pledged
    private final JTextField amountField = new JTextField(10);
    
    //Create a text pane to show data and fields
    private final JTextPane textPane = new JTextPane();
    
    //Create a text area to show data in text pane
    private final JTextArea textArea = new JTextArea();
    
    //Create an formatter call labeled output
    private Formatter output;
    
    //Create save button to store records to a text file
    private final JButton doSave = new JButton("Save");
    
    //Create open button to open records from the text file
    private final JButton doOpen = new JButton("Open");
    
    //Create a clear button to clear all fields
    private final JButton doClear = new JButton("Clear Table");
    
    //Create an exit button to exit/close the program
    private final JButton doExit = new JButton("Exit");
    
    //Create Strings for all fields to be called and drawn from text fields
    private String first;
    private String last;
    private String charity1;
    private String amount2;
    
    //Create string for database, the create=true is to tell the server to create the database if it doesnt exist.
    private final String DB_HOST = "jdbc:derby://localhost:1527/TeamBcharity;create=true";
    
    //common SQL string used to retrieve all contacts
    private final String SQL = "SELECT * FROM CONTACTS";
    
    //SQL statement to create contacts table if first run or table does not exist.
    //
    private final String SQL_TBL = "create table CONTACTS (ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
    		+ "firstName varchar(15), "
    		+ "lastName varChar(15), "
    		+ "charity varChar(30), " 
    		+ "amount varChar(10), "
    		+ "UNIQUE(firstName, lastName, charity))";
    
    //String to hold SQL statement to delete all entries in table, essentially completely deleting the table
    private final String SQL_TBLDEL = "drop table CONTACTS";
    
    //Connection to be used for SQL statements on the local derby database
    Connection conn;
    
    //Statment Object for execution of SQL to the aformentioned Connection
    Statement stmt;
    
    //Create String for the filename to be named ContactInformation.txt
    private final String filename = "LearningTeamB.txt";
    
    //Allow Strings from fields to be stacked (input several contacts)
    private final Stack<String> contact = new Stack<>();
    
    //Image Icon with the path
    private final ImageIcon image = ourImage("charities_logo.jpg");
    
    //Label for program image
    private final JLabel charitypPic = new JLabel(image);
    
    

    public LearningTeamB() {
        super("Learning Team B");
    }

    public JMenuBar createMenuBar() {
        
    	//Create a new JMenuBar called menuBar
        JMenuBar menuBar = new JMenuBar();
        
        //Set the program's menu bar
        setJMenuBar(menuBar);

        //Create a submenu (file) on the menubar
        JMenu menuFile = new JMenu("File", true);
        
        //Add the file submenu to the menubar
        menuBar.add(menuFile);

        //Create a menuitem (exit)    
        JMenuItem menuFileExit = new JMenuItem("Exit");
        
        //Add the exit menuitem to the file submenu
        menuFile.add(menuFileExit);
        
        //Command exit menuitem to exit the program
        menuFileExit.setActionCommand("Exit");
        
        //adding local actionlistener to exit.
        menuFileExit.addActionListener(this);

        //Return the menubar
        return menuBar;
    }

    public Container createContentPane() {

        //Create a north (top) panel to hold fields
        JPanel northPanel = new JPanel();
        
        //Set the layout for the north panel to flow
        northPanel.setLayout(new FlowLayout());
        
        //Add text and fields to populate north panel
        northPanel.add(this.firstName);
        northPanel.add(this.firstNameField);
        northPanel.add(this.lastName);
        northPanel.add(this.lastNameField);
        northPanel.add(this.charity);
        northPanel.add(this.charityField);
        northPanel.add(this.amount);
        northPanel.add(this.amountField);

        //Create center JPanel
        JPanel centerPanel = new JPanel();
        
        //Add all buttons to the center panel in the following order:
        //Save, Open, Clear, Exit
        centerPanel.add(this.doSave);
        centerPanel.add(this.doOpen);
        centerPanel.add(this.doClear);
        centerPanel.add(this.doExit);

        //Create action listener for clear button
        doClear.addActionListener(new ActionListener() {
        
        	//Create performed action when clear button is pressed
            @Override
            public void actionPerformed(ActionEvent clear) {
            	//Set all fields and text pane to blank when clear button is pressed
                firstNameField.setText("");
                lastNameField.setText("");
                charityField.setText("");
                amountField.setText("");
                textArea.setText("");
                try {
					executeSQL(SQL_TBLDEL, true);
					checkTable();
				} catch (SQLException e) {
				}
            }
        });

        //Create action listener for save button
        doSave.addActionListener(new ActionListener() {
            
            private boolean returnValue;

            //Create performed action when save button is pressed
            @Override
            public void actionPerformed(ActionEvent save) {
                
            	//Initiate a try catch for the first name field
                    try {
                        //If the first name field is blank, throw an error
                        if (firstNameField.getText().equals("")) {
                            throw new Exception();
                        } //If the first name field is not blank, proceed
                        else {
                            //Capture first name field
                            first = firstNameField.getText();
                            //Remove all spaces
                            first = first.replaceAll(" ", "");
                        }
                        
                    } catch (Exception E) {
                        //Throw error message asking for a valid first name
                        JOptionPane.showMessageDialog(null, "First name Field cannot be blank", "Error", JOptionPane.ERROR_MESSAGE);
                        //Move cursor back to the first name field to repopulate
                        firstNameField.grabFocus();
                        //Exit this try catch and try again
                        return;
                    }

                    //Initiate a try catch for last name field
                    try {
                        //If the last name field is blank, throw an error
                        if (lastNameField.getText().equals("")) 
                            throw new Exception();
                        
                        //Get text from last name field
                        last = lastNameField.getText();
                        //Remove all spaces
                        last = last.replaceAll(" ", "");
                        
                        
                    } catch (Exception E) {
                        //Throw error message for last name
                        JOptionPane.showMessageDialog(null, "Last name field cannot be blank.", "Error", JOptionPane.ERROR_MESSAGE);
                        //Move cursor back to the last field to repopulate
                        lastNameField.grabFocus();
                        //Exit this try catch and try again
                        return;
                    }

                    //Initiate a try catch for Charity Field
                    try {
                        //If charity field is blank, throw error message
                        if (charityField.getText().equals("")) throw new NumberFormatException();
                       
	                    //Get text from charity field
	                    charity1 = charityField.getText();
	                    
	                    //Remove all spaces
	                    charity1 = charity1.replaceAll(" ", "");
                        
                        
                    } //Catch error
                    catch (NumberFormatException NFE) {
                        //Show error dialog
                        JOptionPane.showMessageDialog(null, "Please input a valid Charity", "Error", JOptionPane.ERROR_MESSAGE);
                        //Move cursor back to the charity field to repopulate
                        charityField.grabFocus();
                        //Exit this try catch and try again
                        return;
                    }

                    //Initiate a verification for age checking
                    try {


                        //Parse the amountField text String to a Float for verification
                        amount1 = Float.parseFloat(amountField.getText());
                            


                        //Check to see if the amountField is populated with a number between 0 and 9,999,999
                        if (amount1 < 0.00 || amount1 > 9999999.00) 
                            throw new Exception();
                        
                        amount2 = amountField.getText();
                        
                        
                    } //Catch errors with the amount verification
                    catch (Exception NFE) {
                        System.err.println(NFE);
                        //If the amount field is not within specified limits show an error dialog box
                        JOptionPane.showMessageDialog(null, "Please input a valid amount to pledge (0.00-9999999.00)", "Error", JOptionPane.ERROR_MESSAGE);
                        //Move cursor back to the amount field to repopulate
                        amountField.grabFocus();
                        //Exit this try catch and try again
                        return;
                    }
                

                //Enter try catch statement to format a filename
                try {
                    output = new Formatter(new FileWriter(filename, true));
                } //Catch any error exceptions
                catch (IOException r) {
                    System.err.println("Error: " + r.getMessage());
                }

                //Set the text in the text pane to show that information is being written to the file
                textArea.setText("Adding to database...");
                
                try {
                        try (ResultSet result = executeSQL(SQL, true)) {
                            result.moveToInsertRow();
                            
                            // insert captured values into called column
                            result.updateString("firstName", first);
                            result.updateString("lastName", last);
                            result.updateString("charity", charity1);
                            result.updateString("amount", amount2);
                            
                            //execute the insertion
                            result.insertRow();
                            
                            //Close SQL elements for next use.
                            conn.close();
                            stmt.close();
                        }

                    //clear fields upon successful entry, to discourage accidentally entering the same thing twice.
                    firstNameField.setText("");
                    lastNameField.setText("");
                    charityField.setText("");
                    amountField.setText("");
                    
                    
                } catch (SQLException err) {
                    
                	//if error message contains "duplicate key value," this indicates that there is already an
                	//entry in the table that matches firstname, lastname, and charity. This will alert the user 
                	//of a duplicate entry in the table.
                	if(err.getMessage().indexOf("duplicate key value") > 0)
                    	JOptionPane.showMessageDialog(null, first + " " + last + " " + "Has already made a contribution to " + charity1, "Duplicate", JOptionPane.ERROR_MESSAGE);
                    //If it was not a duplicate error, print error in textarea.
                	else textArea.setText(err.getMessage());                    
                }
                
                //method to populate TextArea with entries currently in database table.
                populateTextarea();
            }
        });

        //Create action listener for open button
        doOpen.addActionListener(new ActionListener() {
            //Create performed action when open button is pressed
            @Override
            public void actionPerformed(ActionEvent open) {
                //Enter try catch to verify that file is there
                try {
                    //Scan the folder for specified filename
                    Scanner input = new Scanner(new File(filename));
                } //Catch error if filename is not found
                catch (FileNotFoundException fileNotFoundException) {
                    //Console output
                    System.err.print(fileNotFoundException);
                    //Create error popup message if the file is not found
                    JOptionPane.showMessageDialog(null, "File Not Found.", "Error", JOptionPane.ERROR_MESSAGE);
                    //Exit the program when file is not found
                    System.exit(1);
                }

                populateTextarea();
            }
        });

        //Create action listener function when exit button is pressed
        doExit.addActionListener(new ActionListener() {
            //Create performed action when exit button is pressed
            @Override
            public void actionPerformed(ActionEvent g) {
                //Exit the program when exit button is pressed
                System.exit(0);
            }
        });

        //Create a south panel
        JPanel southPanel = new JPanel();
        
        //Allow the text pane to have a scroll button
        JScrollPane scrollPane = new JScrollPane(textArea);
        
        //Set text area to be un-editable
        textArea.setEditable(false);
        
        //set text area to have equal-spaced font so Terry doesnt murder the String.format gods.
        textArea.setFont(new Font("Courier New", Font.PLAIN, 12));
        
        //Make the scroll bar permanent in the text pane
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
        //Set the size of the text pane
        scrollPane.setPreferredSize(new Dimension(600, 200));
        
        //Set the text pane to be un-editable
        textPane.setEditable(false);
        
        //Add the text pane with scroll bar to the south panel
        southPanel.add(scrollPane);

        //Create container to hold all panels
        Container c = getContentPane();
        c.setLayout(new BorderLayout(10, 10));
        
        //Add north panel (text and input fields) to the top
        c.add(northPanel, BorderLayout.NORTH);
        
        //Add center panel (buttons) to the middle
        c.add(centerPanel, BorderLayout.CENTER);
        
        //Add south panel (text pane) to the bottom
        c.add(southPanel, BorderLayout.SOUTH);

        //Return to the container
        return c;
    }

    //Create performed action when exit is selected from the bar menu
    @Override
    public void actionPerformed(ActionEvent e) {
        
    	//Get action command for exit
        String arg = e.getActionCommand();

        //This will happen if the red x is pressed
        if ("Exit".equals(arg)) {
            //Exit the program
            System.exit(0);
        }
    }

    
    //Since the code is used multiple times, created a method for the basic execution of SQL to 
    //The DB_HOST. The method takes String SQL as the SQL statement to be executed, and boolean update to determine whether the
    //statement is meant to update the table or just query(true = update)
    //Method throws SQLexception and should be surrounded with try-catch.
    private ResultSet executeSQL(String SQL, boolean update) throws SQLException
    {
    	//Open connection to DB_HOST
    	conn = DriverManager.getConnection(DB_HOST);
    	
    	//createStatement for stmt variable based on boolean update. If update is true, setup stmt for updating the table
    	//if not, setup stmt for a regular query. 
        stmt = (update)? 
        			conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)://update is true
        				conn.createStatement();//update is false
        
        //attempt execution of the statement and assign to temp variable
        ResultSet temp = stmt.executeQuery(SQL);
        
        //return temp variable if all went well.
        return temp;
    }
    
    //method checkTable is used to create the contacts table if it doesn't exist in the TeamBcharity Database.
    void checkTable()
    {
    	try 
    	{
    		//Open connection to DB_HOST
    		conn = DriverManager.getConnection(DB_HOST);
			
    		//default createStatment
    		stmt = conn.createStatement();
		} catch (SQLException e) 
		{
		}
    	
    	try
    	{
    		//attempt basic query of contacts table
    		stmt.executeQuery(SQL);
    		
    	//if there is an issue, we can assume it is because the table does not exist.
    	} catch (SQLException SQLe)
    	{
    		
				try {
					
					//SQL_tbl statment is to create the table with the specified parameters in the SQL_tbl string.
					stmt.execute(SQL_TBL);
				
				//if this fails, program is useless, inform user with fatal error and exit.	
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "FATAL ERROR: Cannot connect/create local table on database.\n" + e.getMessage(), "FATAL ERROR", JOptionPane.ERROR_MESSAGE);
					System.exit(69);
				}
    	}  
    }
    
    //Method used to display current entries in the DB to the user after an action like saving to the database.
    //Also used with the open button, for less code area.
    private void populateTextarea()
    {
    	textArea.setText(String.format("%-15s %-15s %-30s %-10s\n", "First Name","Last Name","Charity","Amount Pledged"));
        
        
        try {
                try (ResultSet result = executeSQL(SQL, false)) {
                    while (result.next()) {
                        
                        //First Name
                        String firstName = result.getString(2).replace(" ", "");
                        //Last Name
                        String lastName = result.getString(3).replace(" ", "");
                        //Charity
                        String charity1 = result.getString(4).replace(" ", "");
                        //Amount Pledged
                        Float amount2 = Float.parseFloat(result.getString(5).replace(" ", ""));
                        //Full String
                        String full = String.format("%-15s %-15s %-30s $%-10.2f\n", firstName, lastName, charity1, amount2);
                        
                        textArea.append(full);
                    }
                    
                    conn.close();
                    stmt.close();
                }
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }
    
    //A method that uses the Image Icon java class to create a URL
    //and retrieve the relative path of the file
    //If the file does not exist the console will return an error statement
    private ImageIcon ourImage(String thePath) {
        URL theURL = getClass().getResource(thePath);
        if (theURL != null) {
            return new ImageIcon(theURL);
        } else {
            System.err.println("Couldn't find file: " + thePath);
            return null;
        }
    }
    
  //Main Statement Runs
    public static void main(String[] args) {
        //Create a JFrame with normal looks
        JFrame.setDefaultLookAndFeelDecorated(false);
        //Run the contact information program
        LearningTeamB m = new LearningTeamB();
        
        //run a check for the table in the local database, and create if needed.
        m.checkTable();
        //Set red x to exit program
        m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Add menubar (with file/exit functionality) to the frame
        m.setJMenuBar(m.createMenuBar());
        //Add the contentpane (north/center/south panels) to the fram
        m.setContentPane(m.createContentPane());
        //Set the default size
        m.setSize(1200, 555);
        //Set the minimum size
        m.setMinimumSize(new Dimension(1400, 555));
        //populate saved entries
        m.populateTextarea();
        //Make the frame visible
        m.setVisible(true);




    }
    
}
