/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CSS422TeamBrev1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;
/**
 *
 * @author jjose_000
 */
public class CustomerRelationshipManagement extends javax.swing.JFrame {

    /**
     * Creates new form CustomerRelationshipManagement
     */
    public CustomerRelationshipManagement() {
        initComponents();
    }
    //Create string for database, the create=true is to tell the server to create the database if it doesnt exist.
    private final String DB_HOST = "jdbc:derby://localhost:1527/TeamBContacts;create=true";
    
    //common SQL string used to retrieve all contacts
    private final String SQL = "SELECT * FROM CONTACTS";
    
    //base SQL statment to update contact info from Jtable ***row needs to be added before execution
    private final String UPDT_SQL = "UPDATE CONTACTS SET Name = ?,"
    							  + "Address = ?,"
    							  + "City = ?,"
    							  + "State/Providence = ?,"
    							  + "Postal Zone = ?,"
    							  + "Country = ?,"
    							  + "Telephone No. = ?,"
    							  + "Cust. No. = ?"
    							  + " WHERE Cust. No. =";
    
    //SQL statement to create user table if it doesn't exist
    private final String SQL_TBL = "CREATE TABLE CONTACTS("
    								+ "Name char(15),"
    								+ "Address char(15),"
    								+ "City char(30),"
    								+ "State/Providence char(10),"
    								+ "Postal Zone char(5),"
    								+ "Country char(3),"
    								+ "Telephone No. char(12),"
    								+ "Cust. No. INTEGER not null primary key)";
    
    
    
    //Statment Object for execution of SQL to the aformentioned Connection
    Statement stmt;
    
    //Connection to be used for SQL statements on the local derby database
    Connection conn;
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        Personal_Information = new javax.swing.JTable();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        txt_Name = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_Address = new javax.swing.JTextField();
        txt_City = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_StProvidence = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_PostalZone = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txt_Country = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txt_Telephone = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_CustomerNum = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        btn_Save = new javax.swing.JButton();
        btn_Open = new javax.swing.JButton();
        btn_ClearTable = new javax.swing.JButton();
        btn_Exit = new javax.swing.JButton();
        btn_MakeTable = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Personal_Information.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Name", "Address", "City", "State/Providence", "Postal Zone", "Country", "Telephone No.", "Cust. No."
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        Personal_Information.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Personal_InformationMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(Personal_Information);

        jLabel1.setText("Name");

        txt_Name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_NameActionPerformed(evt);
            }
        });

        jLabel2.setText("Address");

        jLabel3.setText("City");

        txt_City.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_CityActionPerformed(evt);
            }
        });

        jLabel4.setText("State/Providence");

        jLabel5.setText("Postal Zone");

        txt_PostalZone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_PostalZoneActionPerformed(evt);
            }
        });

        jLabel6.setText("Country");

        jLabel7.setText("Telephone No.");

        txt_Telephone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_TelephoneActionPerformed(evt);
            }
        });

        jLabel8.setText("Cust. No.");

        txt_CustomerNum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_CustomerNumActionPerformed(evt);
            }
        });

        btn_Save.setText("Save");
        btn_Save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SaveActionPerformed(evt);
            }
        });

        btn_Open.setText("Display Table Information");
        btn_Open.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_OpenActionPerformed(evt);
            }
        });

        btn_ClearTable.setText("Clear Table");

        btn_Exit.setText("Exit");
        btn_Exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ExitActionPerformed(evt);
            }
        });

        btn_MakeTable.setText("Make Table");
        btn_MakeTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_MakeTableActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel6)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel7)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel8))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txt_Name, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txt_Address, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txt_City, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txt_StProvidence, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txt_PostalZone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txt_Country, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txt_Telephone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txt_CustomerNum, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 322, Short.MAX_VALUE))
                            .addComponent(jSeparator2)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(114, 114, 114)
                                .addComponent(btn_Save)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_Open)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_ClearTable)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_Exit))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(492, 492, 492)
                                .addComponent(btn_MakeTable)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3, jLabel4, jLabel5, jLabel6, jLabel7, jLabel8, txt_Address, txt_City, txt_Country, txt_CustomerNum, txt_Name, txt_PostalZone, txt_StProvidence, txt_Telephone});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_Name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_City, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_StProvidence, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_PostalZone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Country, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Telephone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_CustomerNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_Save)
                    .addComponent(btn_Open)
                    .addComponent(btn_ClearTable)
                    .addComponent(btn_Exit))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                .addComponent(btn_MakeTable)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_NameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_NameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_NameActionPerformed

    private void txt_CityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_CityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_CityActionPerformed

    private void txt_PostalZoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_PostalZoneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_PostalZoneActionPerformed

    private void txt_TelephoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_TelephoneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_TelephoneActionPerformed

    private void txt_CustomerNumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_CustomerNumActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_CustomerNumActionPerformed

    private void btn_ExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btn_ExitActionPerformed

    private void btn_OpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_OpenActionPerformed
        UpdateTable();
    }//GEN-LAST:event_btn_OpenActionPerformed

    private void btn_SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SaveActionPerformed
        try{
            Connection conn = null;
            PreparedStatement pst=null;
            //Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_HOST);
            
            //name of the JTable is Personal_Information. the variable row captures the click on table.
            int row = Personal_Information.getSelectedRow();
            //executes query when table is clicked
            row = row + 1;//the value is zero(0) and but the first row has to be one(1).

            //This will update the row in a table by adding row number to UPDT_SQL
            String sql = UPDT_SQL + row;
            pst =conn.prepareStatement(sql);

            pst.setString(1,txt_Name.getText());
            pst.setString(2,txt_Address.getText());
            pst.setString(3,txt_City.getText());
            pst.setString(4,txt_StProvidence.getText());
            pst.setString(5,txt_PostalZone.getText());
            pst.setString(6,txt_Country.getText());
            pst.setString(7,txt_Telephone.getText());
            pst.setString(8,txt_CustomerNum.getText());
            
            System.out.println("Saved to Database.");
            pst.execute();
            JOptionPane.showMessageDialog(null, "Saved");
        }
        catch(Exception e)
        {

            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_btn_SaveActionPerformed

    private void Personal_InformationMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Personal_InformationMouseClicked
        try{
            //---------------------------------------
            //Start New code
            Connection conn = null;
            Statement st = null;
            //Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_HOST);
            //---------------------------------------
            st = conn.createStatement();
            //name of the JTable is Personal_Information. the variable row captures the click on table.
            int row = Personal_Information.getSelectedRow();
            //executes query when table is clicked
            row = row + 1;//the value is zero(0) and but the first row has to be one(1).
            //Execute a query
            String sql = "select * from CONTACTS where \"Cust. No.\" =" + row;
            ResultSet rs = st.executeQuery(sql);
            if(rs.next()){

                //String id = rs.getString("USER_ID");
                //txt_ID.setText(id);
                String name = rs.getString("Name");
                txt_Name.setText(name);
                String address = rs.getString("Address");
                txt_Address.setText(address);
                String City = rs.getString("City");
                txt_City.setText(City);
                String StateProv = rs.getString("State/Providence");
                txt_StProvidence.setText(StateProv);
                String PostalZone = rs.getString("Postal Zone");
                txt_PostalZone.setText(PostalZone);
                String Country = rs.getString("Country");
                txt_Country.setText(Country);
                String Telephone = rs.getString("Telephone No.");
                txt_Telephone.setText(Telephone);
                String CustomerNum = rs.getString("Cust. No.");
                txt_CustomerNum.setText(CustomerNum);
                
            }
        }
        catch(SQLException e)
        {

            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_Personal_InformationMouseClicked

    private void btn_MakeTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_MakeTableActionPerformed
        checkTable();
    }//GEN-LAST:event_btn_MakeTableActionPerformed
    //method checkTable is used to create the contacts table if it doesn't exist in the TeamBcharity Database.
    private void checkTable()
    {
    	try 
    	{
    		//Open connection to DB_HOST
    		conn = DriverManager.getConnection(DB_HOST);
			
    		//default createStatment
    		stmt = conn.createStatement();
		} catch (SQLException e) 
		{
		}
    	
    	try
    	{
    		//attempt basic query of contacts table
    		stmt.executeQuery(SQL);
    		
    	//if there is an issue, we can assume it is because the table does not exist.
    	} catch (SQLException SQLe)
    	{
    		
				try {
					
					//SQL_tbl statment is to create the table with the specified parameters in the SQL_tbl string.
					stmt.execute(SQL_TBL);
				
				//if this fails, program is useless, inform user with fatal error and exit.	
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "FATAL ERROR: Cannot connect/create local table on database.\n" + e.getMessage(), "FATAL ERROR", JOptionPane.ERROR_MESSAGE);
					System.exit(69);
				}
    	}  
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CustomerRelationshipManagement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CustomerRelationshipManagement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CustomerRelationshipManagement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CustomerRelationshipManagement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CustomerRelationshipManagement().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Personal_Information;
    private javax.swing.JButton btn_ClearTable;
    private javax.swing.JButton btn_Exit;
    private javax.swing.JButton btn_MakeTable;
    private javax.swing.JButton btn_Open;
    private javax.swing.JButton btn_Save;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField txt_Address;
    private javax.swing.JTextField txt_City;
    private javax.swing.JTextField txt_Country;
    private javax.swing.JTextField txt_CustomerNum;
    private javax.swing.JTextField txt_Name;
    private javax.swing.JTextField txt_PostalZone;
    private javax.swing.JTextField txt_StProvidence;
    private javax.swing.JTextField txt_Telephone;
    // End of variables declaration//GEN-END:variables
    public void UpdateTable(){
        try{
            //Start New code
            Connection conn = null;
            Statement st = null;
            PreparedStatement pst=null;
            //Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_HOST);
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            pst=conn.prepareStatement(SQL);
            Personal_Information.setModel(DbUtils.resultSetToTableModel(rs));
        }
                catch(SQLException e)
        {

            JOptionPane.showMessageDialog(null, e);

        } 
    }
}
